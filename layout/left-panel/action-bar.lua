-- Import necessary AwesomeWM libraries and custom modules for UI elements and functionality
local awful = require('awful')  -- For window management
local beautiful = require('beautiful')  -- For theme variables
local wibox = require('wibox')  -- Widget and layout library
local gears = require('gears')  -- Various utilities
local mat_icon = require('widget.material.icon')  -- Custom icon widget module
local dpi = require('beautiful').xresources.apply_dpi  -- Function for DPI scaling
local icons = require('theme.icons')  -- Icon paths defined in the theme
local TagList = require('widget.tag-list')  -- Custom tag list widget module
local clickable_container = require('widget.material.clickable-container')  -- Custom module for clickable widgets

-- Function to create and return a side panel for a given screen
return function(screen, panel, action_bar_width)
  -- Create a system tray widget
  local systray = wibox.widget.systray()
  systray:set_horizontal(false)  -- Set the tray to vertical layout
  systray:set_base_size(24)  -- Set the icon base size

  -- Create a menu icon widget
  local menu_icon =
    wibox.widget {
    icon = icons.menu,  -- Path to menu icon
    size = dpi(24),  -- Set icon size with DPI scaling
    widget = mat_icon  -- Use the custom material icon widget
  }

  -- Create a clickable home button widget with the menu icon
  local home_button =
    wibox.widget {
    wibox.widget {
      menu_icon,
      widget = clickable_container  -- Make the container clickable
    },
    bg = beautiful.primary.hue_500,  -- Set the background color from the theme
    widget = wibox.container.background  -- Use background container widget for bg color
  }

  -- Assign a click event to the home button to toggle the panel
  home_button:buttons(
    gears.table.join(
      awful.button(
        {},
        1,  -- Left mouse button
        nil,
        function()
          panel:toggle()  -- Toggle the visibility of the panel
        end
      )
    )
  )

  -- Change the menu icon to close icon when the panel is opened
  panel:connect_signal(
    'opened',
    function()
      menu_icon.icon = icons.close
    end
  )

  -- Change the icon back to the menu icon when the panel is closed
  panel:connect_signal(
    'closed',
    function()
      menu_icon.icon = icons.menu
    end
  )

  -- Assemble the action bar widget
  return wibox.widget {
    id = 'action_bar',
    layout = wibox.layout.align.vertical,  -- Use vertical alignment
    forced_width = action_bar_width,  -- Set the width of the action bar
    {
      -- Top section of the action bar
      layout = wibox.layout.fixed.vertical,
      home_button,  -- Add the home button widget
      TagList(screen)  -- Add the custom tag list widget for the given screen
    },
    -- Middle section of the action bar
    nil, -- empty in this case
    -- Alternatively widgets can be added here
    {
      -- Bottom section of the action bar
      layout = wibox.layout.fixed.vertical,
      wibox.container.margin(systray, dpi(10), dpi(10)),  -- Add the system tray with margins
      -- Additional widgets can be added here, e.g
      --require('widget.wifi'),
      --require('widget.battery'),
    }
  }
end
