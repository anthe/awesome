-- Load necessary libraries for window management, appearance customization, and widget creation
local awful = require('awful') -- Handles window management like opening, closing, and organizing windows
local beautiful = require('beautiful') -- Allows for theming and appearance customization
local wibox = require('wibox') -- A library for creating widgets and panels
local apps = require('configuration.apps') -- Configuration for apps, like default apps for certain tasks
local dpi = require('beautiful').xresources.apply_dpi -- Helps adjust for screen resolution, making things look consistent

-- Define a function to create a panel on the left side of the screen
local left_panel = function(screen)
  -- Set the width of the action bar and the content area, adjusting for screen resolution
  local action_bar_width = dpi(42) -- The slim bar part of the panel
  local panel_content_width = dpi(400) -- The wider, main content area of the panel

  -- Create the main panel as a wibox with certain properties like size and position
  local panel = wibox {
    screen = screen, -- Specifies which screen the panel appears on
    width = action_bar_width, -- Initial width is just the action bar
    height = screen.geometry.height, -- Panel stretches top to bottom of the screen
    x = screen.geometry.x, -- Positioned at the left edge of the screen
    y = screen.geometry.y, -- Positioned at the top edge of the screen
    ontop = true, -- Ensures the panel stays above other windows
    bg = beautiful.background.hue_800, -- Background color from the theme settings
    fg = beautiful.fg_normal -- Text color from the theme settings
  }

  -- Initially, the panel is not open (meaning only the action bar is visible)
  panel.opened = false

  -- Make space for the panel so it doesn't get covered by other windows
  panel:struts({
    left = action_bar_width
  })

  -- Create a transparent overlay for when the panel is open, allowing clicks outside to close the panel
  local backdrop = wibox {
    ontop = true, -- This overlay is above other windows
    screen = screen, -- Covers the entire screen
    bg = '#00000000', -- Completely transparent
    type = 'dock',
    x = screen.geometry.x,
    y = screen.geometry.y,
    width = screen.geometry.width,
    height = screen.geometry.height
  }

  -- Function to open a program launcher (Rofi) when the panel opens
  function panel:run_rofi()
    _G.awesome.spawn(apps.default.rofi, false, false, false, false, function()
      panel:toggle() -- Close the panel after the launcher is closed
    end)
  end

  -- Function to open the panel, showing its contents and enabling the backdrop
  local openPanel = function(should_run_rofi)
    panel.width = action_bar_width + panel_content_width -- Expand panel to full width
    backdrop.visible = true -- Show the backdrop
    panel.visible = false -- Refresh the panel's visibility to apply changes
    panel.visible = true
    panel:get_children_by_id('panel_content')[1].visible = true -- Show the content area
    if should_run_rofi then
      panel:run_rofi() -- Optionally open the program launcher
    end
    panel:emit_signal('opened') -- Notify the system that the panel is open
  end

  -- Function to close the panel, hiding its contents and the backdrop
  local closePanel = function()
    panel.width = action_bar_width -- Shrink the panel back to just the action bar
    panel:get_children_by_id('panel_content')[1].visible = false -- Hide the content area
    backdrop.visible = false -- Hide the backdrop
    panel:emit_signal('closed') -- Notify the system that the panel is closed
  end

  -- Toggle the panel open or closed when called
  function panel:toggle(should_run_rofi)
    self.opened = not self.opened -- Change the open/closed state
    if self.opened then
      openPanel(should_run_rofi) -- Open the panel if it was closed
    else
      closePanel() -- Close the panel if it was open
    end
  end

  -- Allows closing the panel by clicking on the backdrop
  backdrop:buttons(
    awful.util.table.join(
      awful.button({}, 1, function()
        panel:toggle() -- Toggle the panel when the backdrop is clicked
      end)
    )
  )

  -- Arrange the panel's internal layout
  panel:setup {
    layout = wibox.layout.align.horizontal,
    nil, -- Placeholder for future additions
    { -- Panel content configuration
      id = 'panel_content',
      bg = beautiful.background.hue_900, -- Background color for the content area
      widget = wibox.container.background,
      visible = false, -- Content area is initially hidden
      forced_width = panel_content_width, -- Set the width of the content area
      { -- The content of the panel goes here, for example, app shortcuts
        require('layout.left-panel.dashboard')(screen, panel),
        layout = wibox.layout.stack
      }
    },
    require('layout.left-panel.action-bar')(screen, panel, action_bar_width) -- The action bar part of the panel
  }

  return panel -- Return the fully configured panel
end

return left_panel -- Make this panel configuration available for use
