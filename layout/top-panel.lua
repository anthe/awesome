-- Require necessary libraries and modules for the Awesome WM configuration.
local awful = require('awful') -- Utilities for window management.
local beautiful = require('beautiful') -- Theming library for Awesome WM.
local wibox = require('wibox') -- Widget and layout library.
local TaskList = require('widget.task-list') -- Custom widget for task list.
local gears = require('gears') -- Utility library for Awesome WM.
local clickable_container = require('widget.material.clickable-container') -- A widget for clickable functionality.
local mat_icon_button = require('widget.material.icon-button') -- A material design icon button widget.
local mat_icon = require('widget.material.icon') -- A material design icon widget.

local dpi = require('beautiful').xresources.apply_dpi -- Function to adjust DPI settings.

local icons = require('theme.icons') -- Custom icons for the theme.

-- Create a text clock widget in 24-hour format with day and month.
local textclock = wibox.widget.textclock('<span font="Roboto Mono bold 9">%H:%M | %d.%m.</span>')

-- Add a calendar widget that pops up when the textclock is clicked.
-- Credits to kylekewley for the original calendar code.
local month_calendar = awful.widget.calendar_popup.month({
  screen = s, -- The screen where the calendar will be shown.
  start_sunday = false, -- Week starts on Monday.
  week_numbers = true -- Show week numbers.
})

month_calendar:attach(textclock) -- Attach calendar to the text clock widget.

-- Wrap the textclock in margins for aesthetic spacing.
local clock_widget = wibox.container.margin(textclock, dpi(13), dpi(13), dpi(8), dpi(8))

-- Create an add button with a plus icon.
local add_button = mat_icon_button(mat_icon(icons.plus, dpi(24)))
-- Set up the click action for the add button to launch the default app for the selected tag.
add_button:buttons(
  gears.table.join(
    awful.button(
      {},
      1, -- Left click action.
      nil,
      function()
        -- Launches the default application for the focused screen's selected tag in bottom-right corner.
        awful.spawn(
          awful.screen.focused().selected_tag.defaultApp,
          {
            tag = _G.mouse.screen.selected_tag,
            placement = awful.placement.bottom_right
          }
        )
      end
    )
  )
)

-- Function to create a layout box widget for each screen.
-- This widget allows changing between different screen layouts.
local LayoutBox = function(s)
  local layoutBox = clickable_container(awful.widget.layoutbox(s)) -- Create a clickable container for the layout box.
  layoutBox:buttons(
    awful.util.table.join(
      awful.button(
        {},
        1, -- Left click to cycle to next layout.
        function()
          awful.layout.inc(1)
        end
      ),
      awful.button(
        {},
        3, -- Right click to cycle to previous layout.
        function()
          awful.layout.inc(-1)
        end
      ),
      awful.button(
        {},
        4, -- Scroll up to cycle to next layout.
        function()
          awful.layout.inc(1)
        end
      ),
      awful.button(
        {},
        5, -- Scroll down to cycle to previous layout.
        function()
          awful.layout.inc(-1)
        end
      )
    )
  )
  return layoutBox
end

-- Function to create the top panel for each screen.
local TopPanel = function(s, offset)
  -- Calculate offset if necessary (for example, to avoid overlapping with another panel).
  local offsetx = 0
  if offset == true then
    offsetx = dpi(42)
  end

  -- Create the top panel widget with specified dimensions and aesthetic properties.
  local panel =
    wibox(
    {
      ontop = true,
      screen = s,
      height = dpi(42),
      width = s.geometry.width - offsetx,
      x = s.geometry.x + offsetx,
      y = s.geometry.y,
      stretch = false,
      bg = beautiful.background.hue_800, -- Background color from the theme.
      fg = beautiful.fg_normal, -- Foreground color from the theme.
      struts = {
        top = dpi(42)
      }
    }
  )

  -- Ensure the panel does not overlap with window tiles by setting struts.
  panel:struts(
    {
      top = dpi(42)
    }
  )

  -- Set up the layout of the panel, including task list, add button, clock, and layout box.
  panel:setup {
    layout = wibox.layout.align.horizontal,
    {
      layout = wibox.layout.fixed.horizontal,
      TaskList(s), -- Left side: task list widget.
      add_button, -- Next to task list: add button.
    },
    nil, -- Center: (empty in this configuration).
    {
      layout = wibox.layout.fixed.horizontal,
      clock_widget, -- Right side: clock widget.
      LayoutBox(s), -- Next to clock: layout box widget for layout management.
    }
  }

  return panel
end

-- Return the TopPanel function so it can be used in the Awesome WM configuration.
return TopPanel
