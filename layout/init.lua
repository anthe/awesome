-- Load necessary libraries for window management and panel layouts
local awful = require('awful') -- Used for managing windows, screens, and more
local left_panel = require('layout.left-panel') -- Custom module for creating a left side panel
local top_panel = require('layout.top-panel') -- Custom module for creating a top panel

-- Set up panels for each screen connected to the computer
awful.screen.connect_for_each_screen(
  function(s)
    -- 's' represents each screen
    if s.index == 1 then
      -- For the primary screen (usually the first one):
      s.left_panel = left_panel(s) -- Add a left panel
      s.top_panel = top_panel(s, true) -- Add a top panel, 'true' might indicate a specific configuration like enabling it
    else
      -- For any additional screens:
      s.left_panel = left_panel(s) -- Add a left panel
      s.top_panel = top_panel(s, true) -- Add a top panel, 'true' again might be a specific configuration
    end
  end
)

-- Function to show or hide the panels based on whether an application is in fullscreen mode
function updateBarsVisibility()
  for s in screen do
    -- Loop through each screen
    if s.selected_tag then
      -- Check if a "tag" (think of it as a virtual desktop or workspace) is selected
      local fullscreen = s.selected_tag.fullscreenMode
      -- Check if the current tag is in fullscreen mode
      s.top_panel.visible = not fullscreen -- Hide the top panel if in fullscreen mode, show it otherwise
      if s.left_panel then
        s.left_panel.visible = not fullscreen -- Same for the left panel
      end
    end
  end
end

-- Listen for when a tag (workspace) is selected and update the visibility of panels accordingly
_G.tag.connect_signal(
  'property::selected',
  function(t)
    updateBarsVisibility()
  end
)

-- Listen for when a client (application window) changes its fullscreen status and update panels
_G.client.connect_signal(
  'property::fullscreen',
  function(c)
    c.screen.selected_tag.fullscreenMode = c.fullscreen -- Update the tag's fullscreen mode based on the client's status
    updateBarsVisibility() -- Then, update the visibility of panels
  end
)

-- Listen for when a fullscreen application is closed and update panels' visibility
_G.client.connect_signal(
  'unmanage',
  function(c)
    if c.fullscreen then
      -- If the closed application was in fullscreen mode:
      c.screen.selected_tag.fullscreenMode = false -- Reset the fullscreen mode for the tag
      updateBarsVisibility() -- Update the panels' visibility
    end
  end
)
