-- The rc.lua file serves as the primary configuration file for AwesomeWM.

-- Basic Environment Setup
-- Sets up the basic environment for configuring AwesomeWM, including loading essential libraries and modules necessary for managing windows, creating widgets, and defining the visual appearance of the window manager:
-- gears: provides utility functions for AwesomeWM
-- awful: is the primary library for managing windows in AwesomeWM
-- wibox: is used for creating widgets and layouts in AwesomeWM
-- beautiful: provides themes and configurations for AwesomeWM
-- autofous module from the awful library: automatically focuses on windows when they appear
local gears = require('gears')
local awful = require('awful')
local wibox = require('wibox')
local beautiful = require('beautiful')
require('awful.autofocus')

-- Hotkeys and Widgets
-- Loads the hotkeys help widget module for applications like VIM when client with a matching name is opened, enhancing user experience by providing quick access to keyboard shortcuts.
require('awful.hotkeys_popup.keys')

-- Theme and Layout Initialization
-- Initializes the theme settings and layout preferences, controlling the visual appearance and overall environment.
beautiful.init(require('theme'))
require('layout')

-- Module Initialization
-- Enhances functionality with additional features:
-- Notifications: Handles desktop notifications.
-- Auto-start: Manages applications that start automatically with Awesome WM.
-- Decorate-client: Customizes window decorations.
-- Exit-screen: Sets up a custom screen displayed on logout or shutdown.
-- Quake-terminal: Implements a drop-down terminal.
require('module.notifications')
require('module.auto-start')
require('module.decorate-client')
require('module.exit-screen')
require('module.quake-terminal')

-- Configuration Setup
-- Applies configurations for clients (windows), tags (virtual desktops), and global keyboard shortcuts, dictating window management, workspace organization, and interaction.
require('configuration.client')
require('configuration.tags')
_G.root.keys(require('configuration.keys.global'))

-- Client Management Signals
-- Configures behavior for newly opened windows, placing them in a 'slave' position to ensure they don't monopolize the main workspace and preventing them from being placed off-screen.
_G.client.connect_signal(
  'manage',
  function(c)
    if not _G.awesome.startup then
      awful.client.setslave(c)
    end

    if _G.awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
      awful.placement.no_offscreen(c)
    end
  end
)

-- Sloppy Focus Behavior
-- Enables "sloppy focus", where moving the mouse cursor over a window automatically focuses on it, reducing the need for clicks.
_G.client.connect_signal(
  'mouse::enter',
  function(c)
    c:emit_signal('request::activate', 'mouse_enter', {raise = true})
  end
)

-- Window Border Color on Focus
-- Changes the border color of windows when they gain or lose focus, helping users keep track of the active window.
_G.client.connect_signal(
  'focus',
  function(c)
    c.border_color = beautiful.border_focus
  end
)
_G.client.connect_signal(
  'unfocus',
  function(c)
    c.border_color = beautiful.border_normal
  end
)

-- Startup and Shutdown Scripts
-- Executes custom scripts on startup and before exiting Awesome WM to handle tasks like setting up the environment or cleaning up. This runs the pywall script on startup
awesome.connect_signal(
    'exit',
    function(args)
        awful.util.spawn('touch ~/.awesome-restart')
    end
)

awesome.connect_signal(
    'startup',
    function(args)
        awful.util.spawn('bash -c "rm ~/.awesome-restart || ~/.bin/mypywal"')
    end
)
