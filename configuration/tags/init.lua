-- Import essential libraries and configurations for AwesomeWM
local awful = require('awful')  -- Provides functionality for window management.
local gears = require('gears')  -- Utility library for AwesomeWM.
local icons = require('theme.icons')  -- Custom module for loading icons.
local apps = require('configuration.apps')  -- Custom module defining default applications.

-- Define a table of tags (workspaces), each with specific properties.
local tags = {
  {
    icon = icons.chrome,  -- Sets the icon for the tag from the icons module.
    type = 'chrome',  -- Custom type identifier, could be used for theme or rules.
    defaultApp = apps.default.browser,  -- Default application to open in this tag.
    screen = 1  -- Assign this tag to screen 1.
  },
  {
    icon = icons.folder,
    type = 'files',
    defaultApp = apps.default.files,
    screen = 1
  },
  {
    icon = icons.lab,
    type = 'cli',
    defaultApp = apps.default.terminal,
    screen = 1
  },
  {
    icon = icons.code,
    type = 'code',
    defaultApp = apps.default.editor,
    screen = 1
  },
  {
    icon = icons.social,
    type = 'social',
    defaultApp = apps.default.email,
    screen = 1
  },
  {
    icon = icons.music,
    type = 'music',
    defaultApp = apps.default.music,
    screen = 1
  },
  {
    icon = icons.game,
    type = 'game',
    defaultApp = apps.default.vm,
    screen = 1
  }
}

-- Define the layout options available within AwesomeWM.
awful.layout.layouts = {
  awful.layout.suit.tile,
  --awful.layout.suit.tile.top,
  --awful.layout.suit.fair,
  awful.layout.suit.floating,
  --awful.layout.suit.fullscreen,
  --awful.layout.suit.max
}

-- Connect a function to be executed for each screen connected to the system.
awful.screen.connect_for_each_screen(function(s)
  -- Iterate over each tag defined earlier and add it to the current screen.
  for i, tag in pairs(tags) do
    awful.tag.add(
      i,  -- Tag identifier.
      {
        icon = tag.icon,  -- Set the tag's icon.
        icon_only = true,  -- Display only the icon (no text labels).
        layout = awful.layout.suit.tile,  -- Default layout for windows in this tag.
        gap_single_client = false,  -- Don't apply gap if only one window is open.
        gap = 6,  -- Set the gap between windows.
        screen = s,  -- Assign the tag to the current screen.
        defaultApp = tag.defaultApp,  -- Define the default app to open.
        selected = i == 1  -- Select the first tag by default.
      }
    )
  end
end)

-- Connect a signal to dynamically adjust the gap between windows based on the layout.
_G.tag.connect_signal(
  'property::layout',
  function(t)
    local currentLayout = awful.tag.getproperty(t, 'layout')
    if currentLayout == awful.layout.suit.max then
      t.gap = 0  -- No gap for maximized layout.
    else
      t.gap = 4  -- Default gap for other layouts.
    end
  end
)
