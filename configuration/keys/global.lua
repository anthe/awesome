local awful = require('awful')
require('awful.autofocus')
local beautiful = require('beautiful')
local hotkeys_popup = require('awful.hotkeys_popup').widget
local modkey = require('configuration.keys.mod').modKey
local altkey = require('configuration.keys.mod').altKey
local apps = require('configuration.apps')


-- Key bindings
local globalKeys =
	awful.util.table.join(

--- System
  awful.key({ "Mod1", "Control" }, "k",
		hotkeys_popup.show_help,
			{description="show shortcuts", group="system"}),
  awful.key({ "Mod1", "Control" }, "r",
		awesome.restart,
			{description = "reload awesome", group = "system"}),
	awful.key({ "Mod1", "Control"   }, "q",
		awesome.quit,
			{description = "quit awesome", group = "system"}),
	awful.key({"Mod1", "Control"},    'Delete',    function()
		awful.util.spawn_with_shell('system-monitoring-center')    end,
			{description = 'taskmanager', group = 'system'}),
	awful.key({"Mod1", "Control"},    's',    function()
		awful.util.spawn_with_shell('systemctl suspend')    end,
			{description = 'suspend', group = 'system'}),

--- print
	awful.key({ "Mod4" },	'Print',	function()
		awful.util.spawn_with_shell(apps.default.select_screenshot)	end,
			{description = 'Mark an area and take screenshot', group = 'print'}),
	awful.key({ "Mod4", "Shift" },	'Print',	function()
		awful.util.spawn_with_shell(apps.default.full_screenshot)	end,
					{description = 'Take screenshot of full monitors', group = 'print'}),
	awful.key({  },	'Print',	function()
		awful.util.spawn_with_shell(apps.default.screenshot)	end,
			{description = 'Take screenshot of active monitor', group = 'print'}),
	--awful.key({ "Mod4"},	'F12',	function()
	 --awful.util.spawn_with_shell(apps.default.moni)	end,
	  --{description = 'toggle monitor', group = 'print'}),

--- focus
	awful.key({"Mod4"}, 'u',
		awful.client.urgent.jumpto,
			{description = 'jump to urgent client', group = 'WM focus'}),
	awful.key({ "Mod4",           }, "j",        function ()
		awful.client.focus.byidx( 1)        end,
			{description = "focus next by index", group = "WM focus"}),
	awful.key({ "Mod4",           }, "k",        function ()
		awful.client.focus.byidx(-1)        end,
			{description = "focus previous by index", group = "WM focus"}),
	awful.key({ "Mod4", }, "h", function ()
		awful.screen.focus_relative( 1) end,
	    {description = "focus the next screen", group = "WM focus"}),
	awful.key({ "Mod4", }, "l", function ()
		awful.screen.focus_relative(-1) end,
	    {description = "focus the previous screen", group = "WM focus"}),
	awful.key({ "Mod4",           }, "Tab",        function ()
		awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
			{description = "go back", group = "WM focus"}),
	awful.key({ "Mod1" },            "Tab",     function ()
		awful.util.spawn("rofi -show window") end,
			{description = "task switcher", group = "WM focus"}),

-- move
	awful.key({ "Mod4", "Shift"   }, "j", function ()
		awful.client.swap.byidx(  1)    end,
			{description = "swap with next client by index", group = "WM move"}),
	awful.key({ "Mod4", "Shift"   }, "k", function ()
		awful.client.swap.byidx( -1)    end,
			{description = "swap with previous client by index", group = "WM move"}),

-- Layout manipulation
	awful.key({ "Mod4",           }, "Right",     function ()
		awful.tag.incmwfact( 0.05)          end,
			{description = "increase master width factor", group = "WM layout"}),
	awful.key({ "Mod4",           }, "Left",     function ()
		awful.tag.incmwfact(-0.05)          end,
			{description = "decrease master width factor", group = "WM layout"}),
	awful.key({ "Mod4", 				  }, "Down",     function ()
		awful.tag.incnmaster( 1, nil, true) end,
			{description = "increase the number of master clients", group = "WM layout"}),
	awful.key({ "Mod4", 				  }, "Up",     function ()
		awful.tag.incnmaster(-1, nil, true) end,
			{description = "decrease the number of master clients", group = "WM layout"}),
	awful.key({ "Mod4", "Shift" }, "Down",     function ()
		awful.tag.incncol( 1, nil, true)    end,
			{description = "increase the number of columns", group = "WM layout"}),
	awful.key({ "Mod4", "Shift" }, "Up",     function ()
		awful.tag.incncol(-1, nil, true)    end,
			{description = "decrease the number of columns", group = "WM layout"}),
	--awful.key({ "Mod4",           }, "space", function ()
		--awful.layout.inc( 1)                end,
			--{description = "select next", group = "WM layout"}),
	awful.key({ "Mod4", "Shift"   }, "space", function ()
		awful.layout.inc(-1)                end,
			{description = "select previous", group = "WM layout"}),

--- launcher
	awful.key({"Mod4"},    'd',    function()
		awful.spawn(awful.screen.focused().selected_tag.defaultApp,
		{tag = _G.mouse.screen.selected_tag,
		placement = awful.placement.bottom_right})    end,
			{description = 'run default program', group = 'launcher'}),
	awful.key({ "Mod4" },            "r",     function ()
		awful.util.spawn("rofi -show drun") end,
			{description = "run rofi", group = "launcher"}),
	awful.key({ "Mod4" },            "b",     function ()
		awful.util.spawn("rofi -show file-browser-extended -file-browser-show-hidden") end,
			{description = "run rofi file browser", group = "launcher"}),
	-- awful.key({ "Mod4" },            "f",     function ()
	-- 	awful.util.spawn("rofi -show GlobalSearch") end,
	-- 		{description = "run rofi search", group = "launcher"}),
	awful.key({ "Mod4" },            "x",     function ()
		awful.spawn(apps.default.terminal) end,
			{description = "run terminal", group = "launcher"}),
	awful.key({"Mod4"},	't',	function()
		_G.toggle_quake()	end,
			{description = 'run quake terminal', group = 'launcher'}),
	awful.key({ "Mod4" },            "F1",     function ()
		awful.spawn(apps.default.chrome) end,
			{description = "run chromium", group = "launcher"}),
	awful.key({ "Mod4" },            "F2",     function ()
		awful.util.spawn("librewolf") end,
			{description = "run librewolf", group = "launcher"}),
	awful.key({ "Mod4" },            "F3",     function ()
		awful.util.spawn("keepassxc") end,
			{description = "run keepassxc", group = "launcher"}),
	awful.key({ "Mod4" },            "F4",     function ()
		awful.util.spawn("xed") end,
			{description = "run texteditor", group = "launcher"}),
	awful.key({ "Mod4" },            "F5",     function ()
		awful.spawn(apps.default.social) end,
			{description = "run signal", group = "launcher"}),
	awful.key({ "Mod4", "Shift" },   "F5",     function ()
		awful.util.spawn("moderndeck") end,
			{description = "run moderndeck", group = "launcher"}),
	awful.key({ "Mod4" },            "F6",     function ()
		awful.util.spawn("sayonara") end,
			{description = "run sayonara", group = "launcher"}),
	awful.key({ "Mod4", "Shift" },   "F6",     function ()
		awful.util.spawn("vivaldi-stable --app=https://social.tchncs.de/getting-started --class=WebApp-Mastodon1226") end,
			{description = "run mastodon", group = "launcher"}),
	awful.key({ "Mod4" },            "F7",     function ()
		awful.util.spawn("steam") end,
			{description = "run steam", group = "launcher"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
  -- Hack to only show tags 1 and 9 in the shortcut window (mod+s)
  local descr_view, descr_toggle, descr_move, descr_toggle_focus
  if i == 1 or i == 9 then
    descr_view = {description = 'view tag #', group = 'WM tag'}
    descr_toggle = {description = 'toggle tag #', group = 'WM tag'}
    descr_move = {description = 'move focused client to tag #', group = 'WM tag'}
    descr_toggle_focus = {description = 'toggle focused client on tag #', group = 'WM tag'}
  end
  globalKeys =
    awful.util.table.join(
    globalKeys,
    -- View tag only.
    awful.key(
      {"Mod4"},
      '#' .. i + 9,
      function()
        local screen = awful.screen.focused()
        local tag = screen.tags[i]
        if tag then
          tag:view_only()
        end
      end,
      descr_view
    ),
    -- Toggle tag display.
    awful.key(
      {"Mod4", 'Control'},
      '#' .. i + 9,
      function()
        local screen = awful.screen.focused()
        local tag = screen.tags[i]
        if tag then
          awful.tag.viewtoggle(tag)
        end
      end,
      descr_toggle
    ),
    -- Move client to tag.
    awful.key(
      {"Mod4", 'Shift'},
      '#' .. i + 9,
      function()
        if _G.client.focus then
          local tag = _G.client.focus.screen.tags[i]
          if tag then
            _G.client.focus:move_to_tag(tag)
          end
        end
      end,
      descr_move
    ),
    -- Toggle tag on focused client.
    awful.key(
      {"Mod4", 'Control', 'Shift'},
      '#' .. i + 9,
      function()
        if _G.client.focus then
          local tag = _G.client.focus.screen.tags[i]
          if tag then
            _G.client.focus:toggle_tag(tag)
          end
        end
      end,
      descr_toggle_focus
    )
  )
end

return globalKeys
