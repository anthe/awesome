-- Load necessary modules for window management, utility functions, and configuration.
local awful = require('awful')
local gears = require('gears')
local client_keys = require('configuration.client.keys')
local client_buttons = require('configuration.client.buttons')

-- Set up rules for window properties and behavior.
awful.rules.rules = {
  -- Default rule for all clients.
  {
    rule = {}, -- This rule applies to all clients.
    properties = {
      focus = awful.client.focus.filter, -- Focus on window under mouse or most recently clicked.
      raise = true, -- Raise window on focus.
      keys = client_keys, -- Key bindings.
      buttons = client_buttons, -- Mouse button actions.
      screen = awful.screen.preferred, -- Prefer current screen for new windows.
      placement = awful.placement.no_offscreen, -- Prevent windows from being placed offscreen.
      floating = false, -- Don't float windows by default.
      maximized = false, -- Don't start windows maximized.
      above = false, -- Don't layer windows above others by default.
      below = false, -- Don't layer windows below others by default.
      ontop = false, -- Don't keep windows on top.
      sticky = false, -- Don't stick windows across all tags/workspaces.
      maximized_horizontal = false, -- Don't start windows maximized horizontally.
      maximized_vertical = false, -- Don't start windows maximized vertically.
    }
  },

  -- Specific rule for Quake-like terminal.
  {
    rule_any = {name = {'QuakeTerminal'}}, -- Matches any window with the name 'QuakeTerminal'.
    properties = {
      skip_decoration = true -- Skip window decorations.
    }
  },

  -- Example rule for mapping specific applications to a tag/screen, that e.g. applications always map on the tag 1 on screen 1.
  -- find class or role via xprop command
  -- {
  --   rule = { class = 'Thunderbird' },
  --   properties = { screen = 1, tag = "1", switchtotag = true }
  -- },

  -- Rules for maximizing certain applications by default.
  {
    rule = { class = 'VirtualBox Manager' },
    properties = { maximized = true } -- Maximize VirtualBox Manager windows.
  },
  {
    rule = { class = 'VirtualBox Machine' },
    properties = { maximized = true } -- Maximize VirtualBox Machine windows.
  },

  -- Rules for floating and custom shaped windows.
  -- Add any class names for applications to float.
  {
    rule_any = {
      type = {'dialog'}, -- Match dialog windows.
      class = {
        'gnome-calculator',
        'gcr-prompter',
        'Gcr-prompter',
        'file_progress',
        'myxer',
        'xfce4-notifyd',
        'Xfce4-notifyd',
        'Tor Browser'
      }
    },
    properties = {
      placement = awful.placement.centered, -- Center these windows on the screen.
      ontop = true, -- Keep these windows on top.
      floating = true, -- Enable floating for these windows.
      drawBackdrop = true, -- Draw a backdrop (useful for transparent windows).
      shape = function() -- Define a custom window shape.
        return function(cr, w, h)
          gears.shape.rounded_rect(cr, w, h, 8) -- Rounded rectangle with a radius of 8 pixels.
        end
      end,
      skip_decoration = true -- Skip window decorations.
    }
  }
}
