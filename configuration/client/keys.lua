local awful = require('awful')
require('awful.autofocus')
local modkey = require('configuration.keys.mod').modKey
local altkey = require('configuration.keys.mod').altKey

local clientKeys =
  awful.util.table.join(
    awful.key({ "Mod4",   },		 "q",
      function (c)
        c:kill()
      end,
    {description = "close", group = "WM client"}),

    awful.key({ "Mod4",   "Shift" }, "Right",
      function
        (c) c:move_to_screen()
      end,
    {description = "move to screen", group = "WM move"}),

    awful.key({ "Mod4",   "Shift" }, "Left",
      function
        (c) c:move_to_screen()
      end,
    {description = "move to screen", group = "WM move"}),

    awful.key({ "Mod4",   "Shift" }, "h",
      function (c)
        c:move_to_screen()
      end,
    {description = "move to screen", group = "WM move"}),

    awful.key({ "Mod4",   "Shift" }, "l",
      function (c)
        c:move_to_screen()
      end,
    {description = "move to screen", group = "WM move"}),

    awful.key({ "Mod4", },    "m",
      function (c)
        c:swap(awful.client.getmaster())
      end,
    {description = "move to master", group = "WM move"}),

    awful.key({"Mod1",    },     "space",
      function (c)
        c.fullscreen = not c.fullscreen
        c:raise()
      end,
    {description = 'toggle fullscreen', group = 'WM client'}),

    awful.key({ "Mod4",     }, "space",
      function (c)
        c.maximized = not c.maximized
        c:raise()
      end ,
    {description = "(un)maximize", group = "WM client"}),

    awful.key({ "Mod4", "Control" }, "space",              function ()
      local c = awful.client.restore()
        if c then
        c:emit_signal("request::activate", "key.unminimize", {raise = true})
        end
      end,
        {description = "restore minimized", group = "WM client"})
)

return clientKeys
