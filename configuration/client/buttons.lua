-- Load the `awful` module for window management and screen layout functionalities.
local awful = require('awful')

-- Load the configuration for the modification key (e.g., Super or Alt).
local modkey = require('configuration.keys.mod').modKey

-- Combine multiple mouse button configurations for different actions.
return awful.util.table.join(
  -- Mouse button 1 (left click) without modification key: Focuses and raises the clicked window.
  awful.button(
    {}, -- No modification key needed
    1,  -- Mouse button 1 (left click)
    function(c)
      _G.client.focus = c -- Sets focus to the clicked window.
      c:raise() -- Raises the clicked window to the forefront.
    end
  ),

  -- Mouse button 1 (left click) with modification key: Allows moving the focused window.
  awful.button(
    {modkey}, -- Modification key is needed
    1,        -- Mouse button 1 (left click)
    awful.mouse.client.move -- Initiates window moving.
  ),

  -- Mouse button 3 (right click) with modification key: Allows resizing the focused window.
  awful.button(
    {modkey}, -- Modification key is needed
    3,        -- Mouse button 3 (right click)
    awful.mouse.client.resize -- Initiates window resizing.
  ),

  -- Mouse button 4 (mouse wheel up) with modification key: Switches to the next window layout.
  awful.button(
    {modkey}, -- Modification key is needed
    4,        -- Mouse button 4 (mouse wheel up)
    function()
      awful.layout.inc(1) -- Switches to the next layout.
    end
  ),

  -- Mouse button 5 (mouse wheel down) with modification key: Switches to the previous window layout.
  awful.button(
    {modkey}, -- Modification key is needed
    5,        -- Mouse button 5 (mouse wheel down)
    function()
      awful.layout.inc(-1) -- Switches to the previous layout.
    end
  )
)
