local filesystem = require('gears.filesystem')

-- Thanks to jo148 on github for making rofi dpi aware!
local with_dpi = require('beautiful').xresources.apply_dpi
local get_dpi = require('beautiful').xresources.get_dpi
local rofi_command = 'env /usr/bin/rofi -dpi ' .. get_dpi() .. ' -width ' .. with_dpi(400) .. ' -show drun -theme ' .. filesystem.get_configuration_dir() .. '/configuration/rofi.rasi -run-command "/bin/bash -c -i \'shopt -s expand_aliases; {cmd}\'"'

return {
  -- List of apps to start by default on some actions
  default = {
    rofi = rofi_command,
    browser = 'vivaldi-stable',
    files = 'nemo /home/anthe/desk',
    terminal = 'alacritty',
    editor = 'pulsar',
    email = 'betterbird',
    music = 'vivaldi-stable --app=https://soundcloud.com/stream --class=WebApp-Soundcloud4450',
    game = 'steam',
    quake = 'xfce4-terminal',
    social = 'signal-desktop',
    chrome = 'chromium --incognito',
    vm = 'teamviewer',
    screenshot = 'flameshot screen -p ~/pics/screenshots',
    delayed_screenshot = 'flameshot screen -p ~/pics/screenshots -d 5000',
    select_screenshot = 'flameshot gui -p "/home/anthe/pics/screenshots" --accept-on-select',
    full_screenshot = 'flameshot full -p ~/pics/screenshots'
  },

  -- List of apps to start once on start-up
  run_on_start_up = {
    'bauh-tray',
    'blueman-applet',
    --'dwall -s timezone &',
    --'kdeconnect-indicator',
    --'nm-applet --indicator',
    --'numlockx on',
    'picom --config ~/.config/picom/picom.conf',
    '/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 & eval $(gnome-keyring-daemon -s --components=pkcs11,secrets,ssh,gpg)', -- credential manager
    'volumeicon',
    'vorta',
    'keepassxc',
    'xfce4-power-manager',
    'mictray',
    'coffeecell',

  -- Add applications that need to be killed between reloads to avoid multipled instances, inside the awspawn script
    '~/.config/awesome/configuration/awspawn' -- Spawn "dirty" apps that can linger between sessions
  }
}
