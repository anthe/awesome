-- Load necessary libraries for managing window appearance and behavior in AwesomeWM
local awful = require('awful') -- For window management
local gears = require('gears') -- For utilities like timers and shapes
local beautiful = require('beautiful') -- For theming

-- Function to set the rendering mode of a client (window)
local function renderClient(client, mode)
  -- If the client should not be decorated or is already in the desired mode, do nothing
  if client.skip_decoration or (client.rendering_mode == mode) then
    return
  end

  -- Apply the specified mode to the client and reset properties to ensure it's not floating, maximized, etc.
  client.rendering_mode = mode
  client.floating = false
  client.maximized = false
  client.above = false
  client.below = false
  client.ontop = false
  client.sticky = false
  client.maximized_horizontal = false
  client.maximized_vertical = false

  -- If the mode is 'maximized', remove the window border and use a rectangle shape
  if client.rendering_mode == 'maximized' then
    client.border_width = 0
    client.shape = function(cr, w, h)
      gears.shape.rectangle(cr, w, h)
    end
  -- If the mode is 'tiled', set the window border and use a rounded rectangle shape
  elseif client.rendering_mode == 'tiled' then
    client.border_width = beautiful.border_width
    client.shape = function(cr, w, h)
      gears.shape.rounded_rect(cr, w, h, 8)
    end
  end
end

-- Flag to avoid redundant calls
local changesOnScreenCalled = false

-- Function to apply changes to all clients on the current screen based on layout or number of clients
local function changesOnScreen(currentScreen)
  -- Determine if the current layout is maximized or if there's only one client to manage
  local tagIsMax = currentScreen.selected_tag ~= nil and currentScreen.selected_tag.layout == awful.layout.suit.max
  local clientsToManage = {}

  -- Gather all clients that should not be skipped and are not hidden
  for _, client in pairs(currentScreen.clients) do
    if not client.skip_decoration and not client.hidden then
      table.insert(clientsToManage, client)
    end
  end

  -- Set the mode for clients based on the current layout or number of clients
  if (tagIsMax or #clientsToManage == 1) then
    currentScreen.client_mode = 'maximized'
  else
    currentScreen.client_mode = 'tiled'
  end

  -- Apply the determined mode to all manageable clients
  for _, client in pairs(clientsToManage) do
    renderClient(client, currentScreen.client_mode)
  end
  changesOnScreenCalled = false
end

-- Callback function for when a client's state changes (e.g., created, destroyed, hidden)
function clientCallback(client)
  if not changesOnScreenCalled then
    if not client.skip_decoration and client.screen then
      changesOnScreenCalled = true
      local screen = client.screen
      gears.timer.delayed_call(
        function()
          changesOnScreen(screen)
        end
      )
    end
  end
end

-- Callback function for when a tag's state changes (e.g., selected, layout changed)
function tagCallback(tag)
  if not changesOnScreenCalled then
    if tag.screen then
      changesOnScreenCalled = true
      local screen = tag.screen
      gears.timer.delayed_call(
        function()
          changesOnScreen(screen)
        end
      )
    end
  end
end

-- Connect the client and tag callback functions to their respective signals
_G.client.connect_signal('manage', clientCallback)
_G.client.connect_signal('unmanage', clientCallback)
_G.client.connect_signal('property::hidden', clientCallback)
_G.client.connect_signal('property::minimized', clientCallback)
_G.client.connect_signal('property::fullscreen', function(c)
    if c.fullscreen then
      renderClient(c, 'maximized') -- Directly render maximized clients without waiting for delayed call
    else
      clientCallback(c)
    end
  end)

_G.tag.connect_signal('property::selected', tagCallback)
_G.tag.connect_signal('property::layout', tagCallback)
