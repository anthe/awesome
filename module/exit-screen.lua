-- Import necessary libraries for widget creation and window management
local awful = require('awful') -- For executing system commands
local gears = require('gears') -- For general utility functions and objects
local wibox = require('wibox') -- Widget and layout library
local beautiful = require('beautiful') -- Theming library
local icons = require('theme.icons') -- Custom icons for the exit screen
local clickable_container = require('widget.material.clickable-container') -- Makes widgets clickable
local apps = require('configuration.apps') -- User-defined applications and system commands
local dpi = require('beautiful').xresources.apply_dpi -- DPI scaling function

-- Set the icon size for the exit screen buttons, with a default value or scaled by DPI
local icon_size = beautiful.exit_screen_icon_size or dpi(140)

-- Function to create a button widget with an icon
local buildButton = function(icon)
  -- Creates a button with padding, circular shape, and makes it clickable
  local abutton =
    wibox.widget {
    wibox.widget {
      wibox.widget {
        wibox.widget {
          image = icon, -- The icon image for the button
          widget = wibox.widget.imagebox
        },
        top = dpi(16),
        bottom = dpi(16),
        left = dpi(16),
        right = dpi(16),
        widget = wibox.container.margin -- Padding around the icon
      },
      shape = gears.shape.circle, -- Makes the button circular
      forced_width = icon_size,
      forced_height = icon_size,
      widget = clickable_container -- Makes the container clickable
    },
    left = dpi(24),
    right = dpi(24),
    widget = wibox.container.margin -- Outer margin for spacing between buttons
  }

  return abutton -- Return the constructed button
end

-- Function definitions for different power commands
function suspend_command()
  exit_screen_hide() -- Hide the exit screen
  awful.spawn.with_shell(apps.default.lock .. ' & systemctl suspend') -- Lock the screen and suspend
end

function exit_command()
  _G.awesome.quit() -- Quit AwesomeWM
end

function lock_command()
  exit_screen_hide() -- Hide the exit screen
  awful.spawn.with_shell('sleep 1 && ' .. apps.default.lock) -- Lock the screen after a delay
end

function poweroff_command()
  awful.spawn.with_shell('poweroff') -- Power off the system
  awful.keygrabber.stop(_G.exit_screen_grabber) -- Stop listening for key events
end

function reboot_command()
  awful.spawn.with_shell('reboot') -- Reboot the system
  awful.keygrabber.stop(_G.exit_screen_grabber) -- Stop listening for key events
end

-- Create button widgets using the buildButton function and icons
local poweroff = buildButton(icons.power, 'Shutdown')
local reboot = buildButton(icons.restart, 'Restart')
local suspend = buildButton(icons.sleep, 'Sleep')
local exit = buildButton(icons.logout, 'Logout')
local lock = buildButton(icons.lock, 'Lock')

-- Connect the button widgets to their respective command functions on click
poweroff:connect_signal('button::release', function() poweroff_command() end)
reboot:connect_signal('button::release', function() reboot_command() end)
suspend:connect_signal('button::release', function() suspend_command() end)
exit:connect_signal('button::release', function() exit_command() end)
lock:connect_signal('button::release', function() lock_command() end)

-- Get the geometry of the currently focused screen to properly size the exit screen
local screen_geometry = awful.screen.focused().geometry

-- Create the exit screen widget with predefined dimensions and appearance settings
exit_screen =
  wibox({
    x = screen_geometry.x,
    y = screen_geometry.y,
    visible = false, -- Initially hidden
    ontop = true, -- Render above other windows
    type = 'splash', -- Splash screen type, for special windows like this
    height = screen_geometry.height,
    width = screen_geometry.width
  })

-- Set background and foreground colors from the theme, with fallback defaults
exit_screen.bg = beautiful.background.hue_800 .. 'dd'
exit_screen.fg = beautiful.exit_screen_fg or beautiful.wibar_fg or '#FEFEFE'

local exit_screen_grabber -- Declare a variable for key event listening

-- Function to hide the exit screen and stop listening for key events
function exit_screen_hide()
  awful.keygrabber.stop(exit_screen_grabber)
  exit_screen.visible = false
end

-- Function to show the exit screen and start listening for key events
function exit_screen_show()
  exit_screen_grabber =
    awful.keygrabber.run(function(_, key, event)
      if event == 'release' then return end -- Ignore key release events

      -- Map key presses to their respective commands, hiding the screen if necessary
      if key == 's' then
        suspend_command()
      elseif key == 'e' then
        exit_command()
      elseif key == 'l' then
        lock_command()
      elseif key == 'p' then
        poweroff_command()
      elseif key == 'r' then
        reboot_command()
      elseif key == 'Escape' or key == 'q' or key == 'x' then
        exit_screen_hide()
      end
    end)
  exit_screen.visible = true -- Show the exit screen
end

-- Set up mouse buttons to hide the exit screen on middle or right click
exit_screen:buttons(gears.table.join(
    awful.button({}, 2, function() exit_screen_hide() end),
    awful.button({}, 3, function() exit_screen_hide() end)
))

-- Arrange the buttons on the exit screen using a horizontal layout
exit_screen:setup {
  nil,
  {
    nil,
    {
      poweroff,
      reboot,
      suspend,
      exit,
      lock,
      layout = wibox.layout.fixed.horizontal
    },
    nil,
    expand = 'none',
    layout = wibox.layout.align.horizontal
  },
  nil,
  expand = 'none',
  layout = wibox.layout.align.vertical
}
