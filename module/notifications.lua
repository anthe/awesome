-- Load necessary libraries for notification and theming
local naughty = require('naughty') -- Used for displaying notifications on the desktop
local beautiful = require('beautiful') -- Accesses theming properties, such as colors, fonts, etc.
local gears = require('gears') -- Utility library for Awesome WM
local dpi = require('beautiful').xresources.apply_dpi -- Function for DPI scaling, making elements look correct regardless of screen resolution

-- Set up default padding and spacing for notifications
naughty.config.padding = 8 -- Space inside the notification box
naughty.config.spacing = 8 -- Space between multiple notifications

-- Set up default properties for all notifications
naughty.config.defaults.timeout = 5 -- Notifications disappear after 5 seconds
naughty.config.defaults.screen = 1 -- Notifications appear on the first monitor
naughty.config.defaults.position = 'bottom_left' -- Notifications pop up at the bottom left of the screen
naughty.config.defaults.margin = dpi(16) -- Margin around the notification content, scaled for screen DPI
naughty.config.defaults.ontop = true -- Notifications appear above other windows
naughty.config.defaults.font = 'Roboto Regular 10' -- Default font for notification text
naughty.config.defaults.icon = nil -- Default icon for notifications (none)
naughty.config.defaults.icon_size = dpi(32) -- Size for icons in notifications, scaled for screen DPI
naughty.config.defaults.shape = gears.shape.rounded_rect -- Shape of the notification box (rounded rectangle)
naughty.config.defaults.border_width = 0 -- Width of the notification border (0 for no border)
naughty.config.defaults.hover_timeout = nil -- Time for which a notification remains visible after mouse hover (nil for default behavior)

-- Handling startup errors
if _G.awesome.startup_errors then
  -- If there are any errors captured during Awesome WM's startup, display them in a critical notification
  naughty.notify({
    preset = naughty.config.presets.critical, -- Use the 'critical' preset style for the notification
    title = 'Oops, there were errors during startup!', -- Notification title
    text = _G.awesome.startup_errors -- The actual error message(s)
  })
end

-- Handling runtime errors
do
  local in_error = false -- Flag to prevent error handling recursion
  _G.awesome.connect_signal('debug::error', function(err)
    -- When an error signal is received...
    if in_error then return end -- If we're already handling an error, exit to avoid recursion
    in_error = true -- Set flag to indicate error handling is in progress

    -- Display the error in a critical notification
    naughty.notify({
      preset = naughty.config.presets.critical, -- Use the 'critical' preset style
      title = 'Oops, an error happened!', -- Notification title
      text = tostring(err) -- Convert the error object to a string and display it
    })
    in_error = false -- Reset the flag after handling the error
  end)
end

-- Function to log custom messages using the notification system
function log_this(title, txt)
  -- Display a notification with a custom title and text
  naughty.notify({
    title = 'log: ' .. title, -- Prefix the title with 'log: '
    text = txt -- The message to be displayed
  })
end
