-- MODULE AUTO-START
-- Run all the apps listed in configuration/apps.lua as run_on_start_up only once when awesome start

-- Load the required libraries/modules.
local awful = require('awful') -- `awful` is a library for managing windows and screen layouts in AwesomeWM.
local apps = require('configuration.apps') -- `apps` contains configuration settings, specifically applications to run on startup.

-- Define a function named `run_once` to start an application only if it's not already running.
-- The function takes one argument: `cmd`, which is the command line to start the application.
local function run_once(cmd)
  -- The command to search for. Initially, it's the same as `cmd`.
  local findme = cmd
  -- Look for the first space in the command. This is used to extract just the application name from a full command.
  -- For example, from "firefox --new-window", it extracts "firefox".
  local firstspace = cmd:find(' ')
  if firstspace then
    -- If a space is found, adjust `findme` to only include the text up to the first space.
    findme = cmd:sub(0, firstspace - 1)
  end
  -- Run the application if it's not already running.
  -- This is done by using `pgrep` to look for the process. If it's not found, the command (`cmd`) is executed.
  -- `pgrep -u $USER -x` searches for processes by the current user (`$USER`) and exactly matching the command (`-x`).
  awful.spawn.with_shell(string.format('pgrep -u $USER -x %s > /dev/null || (%s)', findme, cmd))
end

-- Iterate through the list of applications defined in `apps.run_on_start_up`.
-- For each application, call `run_once` to start it if it's not already running.
for _, app in ipairs(apps.run_on_start_up) do
  run_once(app)
end
