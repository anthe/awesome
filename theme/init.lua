-- Load necessary libraries and modules.
local gtable = require('gears.table') -- Utility functions for handling tables.
local default_theme = require('theme.default-theme') -- Load the default theme settings.

-- PICK THEME HERE
-- Replace 'PapyElGringo-theme' with the name of your desired theme.
local theme = require('theme.PapyElGringo-theme') -- Load a custom theme, chosen by the user.

local final_theme = {} -- Initialize an empty table to store the final theme settings.

-- Merge settings from the default theme into the final theme.
-- 'crush' function merges two tables. If the same key exists in both, the second table's value will overwrite the first's.
gtable.crush(final_theme, default_theme.theme)

-- Then, merge settings from the chosen theme into the final theme, potentially overwriting defaults.
gtable.crush(final_theme, theme.theme)

-- Apply any Awesome WM specific overrides from the default theme.
-- These overrides can modify the theme settings further, based on the window manager's requirements or preferences.
default_theme.awesome_overrides(final_theme)

-- Apply any Awesome WM specific overrides from the chosen theme.
-- This allows the chosen theme to make final adjustments or overrides to the theme settings.
theme.awesome_overrides(final_theme)

-- Return the fully merged and adjusted theme as the final theme configuration to be used by Awesome WM.
return final_theme
