-- Load necessary libraries.
local filesystem = require('gears.filesystem') -- Access to filesystem operations.
local mat_colors = require('theme.mat-colors') -- Import predefined material colors.
local theme_dir = filesystem.get_configuration_dir() .. '/theme' -- Define the directory where the theme is located.
local dpi = require('beautiful').xresources.apply_dpi -- Function for DPI scaling, making elements look correct regardless of screen resolution.

-- Initialize the theme table where we'll set all our theme configurations.
local theme = {}

-- Path configurations
theme.icons = theme_dir .. '/icons/' -- Directory where the theme's icons are stored.
theme.font = 'Ubuntu medium 10' -- Default font for the theme.

-- Color Palette
-- Primary: The main color used throughout the theme (e.g., for headers).
theme.primary = mat_colors.indigo -- Set the primary color to indigo from the material colors library.
theme.primary.hue_500 = '#076678' -- A specific shade of the primary color.

-- Accent: Secondary color used for highlights and actions.
theme.accent = mat_colors.pink -- Set the accent color to pink from the material colors library.

-- Background: Colors used mainly for the window manager's background.
theme.background = mat_colors.blue_grey -- Background color set to blue-grey from the material colors library.
theme.background.hue_800 = '#3c3839' -- Darker shade of the background color.
theme.background.hue_900 = '#282828' -- Even darker shade, often used for emphasizing depth or for darker themes.

-- Function for overriding default Awesome WM theme settings (if necessary).
-- This function doesn't change anything by itself but provides a structure for modifications.
local awesome_overrides = function(theme)
  -- This function is left empty but can be filled with commands to modify the theme further.
end

-- Return the configured theme and any overrides to be used by Awesome WM.
return {
  theme = theme,
  awesome_overrides = awesome_overrides
}
