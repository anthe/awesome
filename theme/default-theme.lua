-- Required libraries for handling files, colors, and scaling.
local filesystem = require('gears.filesystem') -- To work with the file system.
local mat_colors = require('theme.mat-colors') -- Material design color palette.
local theme_dir = filesystem.get_configuration_dir() .. '/theme' -- The directory where the theme resources are stored.
local gears = require('gears') -- For various utilities like shapes.
local dpi = require('beautiful').xresources.apply_dpi -- For DPI scaling, to ensure UI elements have the correct size.

-- Initializing the theme table where we'll store all our configuration.
local theme = {}
theme.icons = theme_dir .. '/icons/' -- Path to the icons used in the theme.
theme.font = 'Ubuntu Medium 10' -- The default font for the theme.

-- Color palettes for the theme.
-- Primary color used prominently throughout the user interface.
theme.primary = mat_colors.deep_orange
-- Accent color for highlighting elements.
theme.accent = mat_colors.pink
-- Background color for the overall UI.
theme.background = mat_colors.grey

-- Function to override or add additional settings to the theme.
local awesome_overrides = function(theme)
  theme.dir = os.getenv('HOME') .. '/.config/awesome/theme' -- Path to the theme directory.
  theme.icons = theme.dir .. '/icons/' -- Update the path to the icons.
  theme.font = 'Ubuntu Medium 10' -- Default font for text.
  theme.title_font = 'Ubuntu Medium 14' -- Font for window titles.

  -- Text colors for various states.
  theme.fg_normal = '#fbfac7' -- Color for regular text.
  theme.fg_focus = '#e4e4e4' -- Color for text on focused elements.
  theme.fg_urgent = '#CC9393' -- Color for text requiring attention.
  theme.bat_fg_critical = '#232323' -- Color for critical battery status.

  -- Background colors for various states.
  theme.bg_normal = theme.background.hue_800 -- Regular background color.
  theme.bg_focus = '#5a5a5a' -- Background color when an element is focused.
  theme.bg_urgent = '#3F3F3F' -- Background color for urgent notifications.
  theme.bg_systray = theme.background.hue_800 -- Background color for the system tray.

  -- Border settings.
  theme.border_width = dpi(2) -- Width of window borders.
  theme.border_normal = theme.background.hue_800 -- Color of normal borders.
  theme.border_focus = '#83a598' -- Color of focused window borders.
  theme.border_marked = '#CC9393' -- Color of marked windows, for special attention.

  -- Menu settings.
  theme.menu_height = dpi(16) -- Height of the menu.
  theme.menu_width = dpi(160) -- Width of the menu.

  -- Tooltip settings.
  theme.tooltip_bg = '#232323' -- Background color for tooltips.
  theme.tooltip_border_width = 0 -- Border width for tooltips.
  theme.tooltip_shape = function(cr, w, h) -- Shape of the tooltips.
    gears.shape.rounded_rect(cr, w, h, dpi(6))
  end

  -- Layout icons.
  theme.layout_max = theme.icons .. 'layouts/arrow-expand-all.png' -- Maximized layout icon.
  theme.layout_tile = theme.icons .. 'layouts/view-quilt.png' -- Tiled layout icon.
  theme.layout_floating = theme.icons .. 'layouts/floatingw.png' -- Floating layout icon.

  -- Taglist settings.
  theme.taglist_bg_empty = theme.background.hue_800 -- Background color for empty tags.
  -- Gradient background for occupied tags.
  theme.taglist_bg_occupied = 'linear:0,0:' .. dpi(48) .. ',0:0,' .. theme.primary.hue_500 .. ':0.08,' .. theme.primary.hue_500 .. ':0.08,' .. theme.background.hue_800 .. ':1,' .. theme.background.hue_800
  -- Gradient background for urgent tags.
  theme.taglist_bg_urgent = 'linear:0,0:' .. dpi(48) .. ',0:0,' .. '#d79921' .. ':0.08,' .. '#d79921' .. ':0.08,' .. theme.background.hue_800 .. ':1,' .. theme.background.hue_800
  -- Gradient background for focused tags.
  theme.taglist_bg_focus = 'linear:0,0:' .. dpi(48) .. ',0:0,' .. '#fbfac7' .. ':0.08,' .. '#fbfac7' .. ':0.08,' .. theme.background.hue_800 .. ':1,' .. theme.background.hue_800

  -- Tasklist settings.
  theme.tasklist_font = 'Ubuntu Medium 11' -- Font for the tasklist.
  theme.tasklist_bg_normal = theme.background.hue_800 -- Background color for normal tasks.
  -- Gradient background for focused tasks.
  theme.tasklist_bg_focus = 'linear:0,0:0,' .. dpi(48) .. ':0,' .. theme.background.hue_800 .. ':0.95,' .. theme.background.hue_800 .. ':0.95,' .. theme.fg_normal .. ':1,' .. theme.fg_normal
  theme.tasklist_bg_urgent = theme.primary.hue_800 -- Background color for urgent tasks.
  theme.tasklist_fg_focus = '#fbfac7' -- Text color for focused tasks.
  theme.tasklist_fg_urgent = theme.fg_normal -- Text color for urgent tasks.
  theme.tasklist_fg_normal = '#7c6f64' -- Text color for normal tasks.

  theme.icon_theme = 'Papirus-Dark' -- Icon theme for applications.
end

-- Returning the theme table and the override function.
return {
  theme = theme,
  awesome_overrides = awesome_overrides
}
